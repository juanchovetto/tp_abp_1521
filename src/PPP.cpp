#include <PPP.h>
#include <Arduino.h>
#include <URTouch.h>
#include <UTFT.h>
extern uint8_t SmallFont[];
extern uint8_t BigFont[];
extern uint8_t SevenSegNumFont[];
UTFT    myGLCD(SSD1289, PA2, PA3, PA1, PA0); 
URTouch  myTouch(PB9, PB8, PB7, PB6, PB5);

void PPP() //Proyectar Pantalla Principal
{
  //Titulo
  myGLCD.setBackColor(0, 0, 0);
  myGLCD.setColor(255, 255, 255);
  myGLCD.setFont(BigFont);
  myGLCD.print("Centro De Informacion", CENTER, 10);
  myGLCD.setColor(0, 0, 255);
  myGLCD.drawLine(0, 32, 480, 32);
  myGLCD.setColor(255, 255, 255);
  myGLCD.setFont(BigFont);
  myGLCD.print("Elija Un Modo", CENTER, 60);

  //Primer Modo
  myGLCD.setColor(16, 167, 103);
  myGLCD.fillRoundRect(100, 120, 380, 160); 
  myGLCD.setColor(255, 255, 255);
  myGLCD.drawRoundRect(100, 120, 380, 160);
  myGLCD.setFont(BigFont);
  myGLCD.setBackColor(16, 167, 103);
  myGLCD.print("Modo Exteriores", CENTER, 132); // Y + 12

  //Segundo Modo

  myGLCD.setColor(16, 167, 103);
  myGLCD.fillRoundRect(100, 190, 380, 230);
  myGLCD.setColor(255, 255, 255);
  myGLCD.drawRoundRect(100, 190, 380, 230);
  myGLCD.setFont(BigFont);
  myGLCD.setBackColor(16, 167, 103);
  myGLCD.print("Modo Interiores", CENTER, 202); // Y + 12

}