#include <Arduino.h>

#include <DHT.h> // Incluimos librería del sensor de humedad

#include <Time.h>  //Inluimos las librerias del RTC
#include <TimeLib.h>

#include <Wire.h>
#include <Adafruit_BMP085.h>  //Incluimos las librerias necesarias para el BMP180

#include <URTouch.h>
#include <UTFT.h>    //Incluimos las librerias de la pantalla TFT
#include <PPP.h>
#include <PE.h>
#include <PIx.h>

#define DHTPIN 12 // Definimos el pin digital donde se conecta el sensor de temperatura
#define DHTTYPE DHT11 // Definimos el tipo de sensor el DHT11 en nuesto caso
int pinLedIn = 14;
int pinLedOut = 13;
int LDRA = A5;
int VoltajeDiv;
// Inicializamos el sensor DHT11
DHT dht(DHTPIN, DHTTYPE);

UTFT    myGLCD(SSD1289, PA2, PA3, PA1, PA0); 
URTouch  myTouch(PB9, PB8, PB7, PB6, PB5);
extern uint8_t SmallFont[];
extern uint8_t BigFont[];
extern uint8_t SevenSegNumFont[];
int x;
int y;
char pantalla;

Adafruit_BMP085 bmp; //llamo a bmp para luego utilizarlo en las medidas requeridas

int h = dht.readHumidity();
int t = dht.readTemperature();

void setup() {

myGLCD.InitLCD(); //Inicializo lcd
myGLCD.clrScr(); //Limpio el lcd
myTouch.InitTouch(); //Inicializo el tactil
myTouch.setPrecision(PREC_HI); //Seteo la precision tactil
PPP();
pantalla = '0';

// Inicializamos la pantalla serie
Serial.begin(9600);

//Comprobar que el sensor esta siendo detectado.
  if (!bmp.begin()) {
	Serial.println("BMP180 Mal conectado / no existente");
	while (1) {}
  }
// Comenzamos el sensor DHT
dht.begin();

}
void loop() {

 VoltajeDiv = analogRead(LDRA);

 //Pantalla Principal
  if(pantalla == '0')
  {
    if(myTouch.dataAvailable())
    {
      myTouch.read();
      x = myTouch.getX();
      y = myTouch.getY();
      if((x >= 100) && (x <= 380) && (y >= 120) && (y <= 160))
      {
        pantalla = '1';
        myGLCD.clrScr();
        PE();
      }
      if((x >= 100) && (x <= 380) && (y >= 190) && (y <= 230))
      {
        pantalla = '2';
        myGLCD.clrScr();
        //aca va la pantalla con todo lo que muestra el modo interior
      }
    }
  }

  //Modo Exteriores
  if(pantalla == '1')
  {
    if(myTouch.dataAvailable())
    {
      myTouch.read();
      x = myTouch.getX();
      y = myTouch.getY();
      //Boton Atras
      if((x >= 10) && (x <= 60) && (y >= 10) && (y <= 40))
      {
        pantalla = '0';
        myGLCD.clrScr();
        PPP();
      }
    }
  }

  //Modo Interiores
  if(pantalla == '2')
  {
    if(myTouch.dataAvailable())
    {
      myTouch.read();
      x = myTouch.getX();
      y = myTouch.getY();
      //Boton Atras
      if((x >= 10) && (x <= 60) && (y >= 10) && (y <= 40))
      {
        pantalla= '0';
        myGLCD.clrScr();
        PPP();
      }
    }
  }
  if (pantalla == '2'){
    digitalWrite (14, HIGH);
    digitalWrite (13, LOW);
  }
  if (pantalla == '1'){
    digitalWrite (14, LOW);
    digitalWrite (13, HIGH);
    }
}