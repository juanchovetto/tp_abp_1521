#include <PPP.h>
#include <PIx.h>
#include <Arduino.h>
#include <URTouch.h>
#include <UTFT.h>
#include <Time.h>  
#include <TimeLib.h>
#include <Wire.h>
#include <Adafruit_BMP085.h>
#define DHTPIN 12 
#define DHTTYPE DHT11
#include <DHT.h>
DHT dht(DHTPIN, DHTTYPE);
int h = dht.readHumidity();
int t = dht.readTemperature();
int LDRA = A5;

extern uint8_t SmallFont[];
extern uint8_t BigFont[];
UTFT    myGLCD(SSD1289, PA2, PA3, PA1, PA0); 
URTouch  myTouch(PB9, PB8, PB7, PB6, PB5);

int VoltajeDiv;

Adafruit_BMP085 bmp;

void loop (){

     VoltajeDiv = analogRead(LDRA);
}
void PE()
{
    //Opcion Humedad

    myGLCD.setBackColor(0, 0, 0);
    myGLCD.setColor(16, 167, 103);
    myGLCD.fillRoundRect(100, 100, 230, 150);
    myGLCD.setColor(255, 255, 255);
    myGLCD.drawRoundRect(100, 100, 230, 150);
    //Falta Agregar Palabra "Humedad: "
    myGLCD.setFont(BigFont);
    myGLCD.setBackColor(16, 167, 103);
    myGLCD.printNumI(h, 165, 125, 3, '0'); //Solo se escribe el valor de humedad
    // Agregar simbolo %

    //Opcion Temperatura

    myGLCD.setColor(16, 167, 103);
    myGLCD.fillRoundRect(250, 100, 380, 150);
    myGLCD.setColor(255, 255, 255);
    myGLCD.drawRoundRect(250, 100, 380, 150);
    //Falta Agregar Palabra "Temperatura: "
    myGLCD.setFont(BigFont);
    myGLCD.setBackColor(16, 167, 103);
    myGLCD.printNumI(t, 315, 125, 3, '0'); //Solo se escribe el valor de Temperatura
    // Agregar *C

    //Opcion Presion Atmosferica

    myGLCD.setColor(16, 167, 103);
    myGLCD.fillRoundRect(100, 170, 230, 220);
    myGLCD.setColor(255, 255, 255);
    myGLCD.drawRoundRect(100, 170, 230, 220);
    //Falta Agregar Palabra "Presion Atmosferica: "
    myGLCD.setFont(BigFont);
    myGLCD.setBackColor(16, 167, 103);
    myGLCD.printNumI(bmp.readPressure(), 165, 195, 3, '0'); //Solo se escribe el valor de Presion Atmosferica
    // Agregar Pascales

    //Opcion Altitud

    myGLCD.setColor(16, 167, 103);
    myGLCD.fillRoundRect(250, 170, 380, 220);
    myGLCD.setColor(255, 255, 255);
    myGLCD.drawRoundRect(250, 170, 380, 220);
    //Falta Agregar Palabra "Altitud: "
    myGLCD.setFont(BigFont);
    myGLCD.setBackColor(16, 167, 103);
    myGLCD.printNumI(bmp.readAltitude(), 315, 195, 3, '0'); //Solo se escribe el valor de Altitud
    // Agregar Metros

    //Fecha

    myGLCD.setColor(255, 255, 255);
    myGLCD.setFont(SmallFont);
    myGLCD.printNumI(day(), 400, 300, 2, '0');
    myGLCD.setColor(255, 255 ,255);
    myGLCD.setFont(SmallFont);
    myGLCD.print("/", 403, 303);
    myGLCD.setColor(255, 255, 255);
    myGLCD.setFont(SmallFont);
    myGLCD.printNumI(month(), 404, 304, 2, '0');
    myGLCD.setColor(255, 255 ,255);
    myGLCD.setFont(SmallFont);
    myGLCD.print("/", 406, 306);
    myGLCD.setColor(255, 255 ,255);
    myGLCD.setFont(SmallFont);
    myGLCD.printNumI(year(), 407, 307);
    }
    